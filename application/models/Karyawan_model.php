<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan_model extends CI_Model
{ 
	//panggil nama tabel
	private $_table = "karyawan";

	public function rules(){
		return
		[
					[ 
						'field'=> 'nik',
						'label' => 'NIK',
						'rules' => 'required|numeric',
						'errors' => [
							'required' => 'NIK tidak Boleh kosong.',
							'numeric'	=> 'NIK harus angka.',
						]
					],
					[
						'field' => 'nama_karyawan',
						'label'  => 'Nama karyawan',
						'rules' => 'required',
						'errors' =>[
							'required' => 'Nama karyawan tidak Boleh kosong.',
						]
					],
					[
						'field' => 'tempat_lahir',
						'label'  => 'Tempat Lahir',
						'rules' => 'required',
						'errors' =>[
							'required' => 'Tempat Lahir tidak Boleh kosong.',
						]
					],
					
					[
						'field' => 'telpon',
						'label'  => 'Telpon',
						'rules' => 'required',
						'errors' =>[
							'required' => 'Telpon tidak Boleh kosong.',
							
						]
					],
					[
						'field' => 'alamat',
						'label'  => 'Alamat',
						'rules' => 'required',
						'errors' =>[
							'required' => 'Alamat tidak Boleh kosong.',
							
						]
					],
					//[
					//	'field' => 'kode_jabatan',
					//	'label'  => 'Jabatan',
					//	'rules' => 'required',
					//	'errors' =>[
					//		'required' => 'Jabatan tidak Boleh kosong.',
							
					//	]
					//]
					//[
					//	'field' => 'stok',
					//	'label'  => 'Stok Barang',
					//	'rules' => 'required|numeric',
					//	'errors' =>[
					//		'required' => 'Stok Barang tidak Boleh kosong.',
					//		'numeric'	=> 'Stok Barang harus angka.',
					//	]
					//]
		];
	}


	public function tampilDataKaryawan()
		{
			//seperti : select * from <nama_table>
			return $this->db->get($this->_table)->result();
		}

	public function tampilDataKaryawan2()
		{
			//KETIKA MAKE QUERY
			$query = $this->db->query("SELECT * FROM karyawan WHERE flag = 1");
			return $query->result();
		}

	public function tampilDataKaryawan3()
		{
			//MAKE QUERY BUILDER
			$this->db->select('*');
			$this->db->order_by('nik', 'ASC');
			$result = $this->db->get($this->_table);
			return $result->result();
		}
	public function save()
		{
			$tgl 		= $this->input->post('tgl');
			$bulan 		= $this->input->post('bulan');
			$tahun 		= $this->input->post('tahun');
			$tgl_gabung = $tahun . "_" . $bulan .  "_" . $tgl;
			
			$data['nik'] 				=$this->input->post('nik');
			$data['nama_lengkap'] 		=$this->input->post('nama_karyawan');
			$data['tempat_lahir'] 		=$this->input->post('tempat_lahir');
			$data['tgl_lahir'] 			=$tgl_gabung;
			$data['jenis_kelamin'] 		=$this->input->post('jenis_kelamin');
			$data['alamat'] 			=$this->input->post('alamat');
			$data['telp'] 				=$this->input->post('telpon');
			$data['kode_jabatan'] 		=$this->input->post('kode_jabatan');
			$data['flag'] 				=1;
			$this->db->insert($this->_table, $data);
			//catetan $data['nama_lengkap']<-(seseuai database) =$this->input->post('nama_karyawan')<-seseuai inputkaryawan;

		}
	public function detail($nik)
	{
		$this->db->select('*');
		$this->db->where('nik', $nik);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}


public function update($nik)
	{
		$tgl = $this->input->post('tgl');
		$bln = $this->input->post('bln');
		$thn = $this->input->post('thn');
		$tgl_gabung = $thn . "-" . $bln . "-" . $tgl;
		
		$data['nama_lengkap']			= $this->input->post('nama_karyawan');
		$data['tempat_lahir']			= $this->input->post('tempat_lahir');
		$data['tgl_lahir']				= $tgl_gabung;
		$data['jenis_kelamin']			= $this->input->post('jenis_kelamin');
		$data['alamat']					= $this->input->post('alamat');
		$data['telp']					= $this->input->post('telp');
		$data['kode_jabatan']			= $this->input->post('kode_jabatan');
		$data['flag']					= 1;
		
		$this->db->where('nik', $nik);
		$this->db->update($this->_table, $data);
	}

	public function delete($nik)
	
		{
			$this->db->where('nik',$nik);
			$this->db->delete($this->_table);
		}

} 