<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Barang_models extends CI_Model
{ 
	//panggil nama tabel
	private $_table = "barang";

	public function rules(){
		return
		[
					[ 
						'field'=> 'kode_barang',
						'label' => 'Kode Barang',
						'rules' => 'required|max_length[5]',
						'errors' => [
							'requered' => 'kode barang tidak boleh kosong.',
							'max_length' => 'kode barang tidak boleh lebih dari 5 karakter.',
						]
					],
					[
						'field' => 'nama_barang',
						'label'  => 'Nama Barang',
						'rules' => 'required',
						'errors' =>[
							'required' => 'Nama Barang tidak Boleh kosong.',
						]
					],
					[
						'field' => 'harga_barang',
						'label'  => 'Harga Barang',
						'rules' => 'required|numeric',
						'errors' =>[
							'required' => 'Harga Barang tidak Boleh kosong.',
							'numeric'	=> 'Harga Barang harus angka.',
						]
					]
					//[
					//	'field' => 'kode_jenis',
					//	'label'  => 'Kode Jenis',
					//	'rules' => 'required',
					//	'errors' =>[
					//		'required' => 'Kode Jenis tidak Boleh kosong.',
							
					//	]
					//],
					//[
					//	'field' => 'stok',
					//	'label'  => 'Stok Barang',
					//	'rules' => 'required|numeric',
					//	'errors' =>[
					//		'required' => 'Stok Barang tidak Boleh kosong.',
					//		'numeric'	=> 'Stok Barang harus angka.',
					//	]
					//]
		];
	}

	public function tampilDataBarang()
		{
			//seperti : select * from <nama_table>
			return $this->db->get($this->_table)->result();
		}

	public function tampilDataBarang2()
		{
			//KETIKA MAKE QUERY
			$query = $this->db->query("SELECT * FROM barang WHERE flag = 1");
			return $query->result();
		}

	public function tampilDataBarang3()
		{
			//MAKE QUERY BUILDER
			$this->db->select('*');
			$this->db->order_by('kode_barang', 'ASC');
			$result = $this->db->get($this->_table);
			return $result->result();
		}
		public function save()
		{
			
			$data['kode_barang'] =$this->input->post('kode_barang');
			$data['nama_barang'] =$this->input->post('nama_barang');
			$data['harga_barang'] =$this->input->post('harga_barang');
			$data['kode_jenis'] =$this->input->post('kode_jenis');
			$data['flag'] =1;
			$this->db->insert($this->_table, $data);
			//catetan $data['nama_lengkap']<-(seseuai database) =$this->input->post('nama_karyawan')<-seseuai inputkaryawan;

		}
		public function detail($kode_barang)
			{
				//$query = $this->db->query("SELECT barang.*, jenis_barang.nama_barang FROM barang INNER JOIN jenis_barang ON barang.kode_jenis = jenis_barang.kode_jenis WHERE barang.flag = 1 AND barang.kode_barang = '$kode_barang'");
				//return $query->result();
				$this->db->select('*');
				$this->db->where('kode_barang', $kode_barang);
				$this->db->where('flag', 1);
				$result = $this->db->get($this->_table);
				return $result->result();
			}

		public function update($kode_barang)
	{
		$data['nama_barang']			= $this->input->post('nama_barang');
		$data['harga_barang']			= $this->input->post('harga_barang');
		$data['kode_jenis']				= $this->input->post('kode_jenis');
		$data['flag']					= 1;
		
		$this->db->where('kode_barang', $kode_barang);
		$this->db->update($this->_table, $data);
	}
	public function delete($kode_barang)
	
	{
		$this->db->where('kode_barang',$kode_barang);
		$this->db->delete($this->_table);
	}

public function updateStok($kd_barang, $qty)
    {
        //panggil data detail stok
        $cari_stok = $this->detail($kd_barang);
        foreach ($cari_stok as $data) {
            $stok=$data->stok;
        }
        
        //proses update stok table barang
        $jumlah_stok     		= $stok + $qty;
        $data_barang['stok']    = $jumlah_stok;
        
        $this->db->where('kode_barang', $kd_barang);
        $this->db->update('barang', $data_barang);
    }

} 