-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 26, 2019 at 06:35 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 7.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `toko_jaya_abadi`
--
CREATE DATABASE IF NOT EXISTS `toko_jaya_abadi` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `toko_jaya_abadi`;

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `kode_barang` varchar(5) NOT NULL,
  `nama_barang` varchar(150) NOT NULL,
  `harga_barang` float NOT NULL,
  `kode_jenis` varchar(5) NOT NULL,
  `flag` int(11) NOT NULL,
  `stock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`kode_barang`, `nama_barang`, `harga_barang`, `kode_jenis`, `flag`, `stock`) VALUES
('22231', 'Printer Canon M789', 450007, 'JN001', 1, 0),
('BR001', 'Laptop Samsung', 4500000, 'JN001', 1, 0),
('BR006', 'Natrium Hipoklorit', 450, 'JN004', 1, 0),
('BR007', 'Printer Canon', 700000, 'JN00', 1, 0),
('BR012', 'Natrium Hipoklorit', 700000, 'JN004', 1, 0),
('BR013', 'Printer Canon', 600000, 'JN002', 1, 0),
('BR027', 'Software CorelDraw', 2000000, 'JN003', 1, 0),
('BR043', 'Natrium Hipoklorit', 450000, 'JN004', 1, 0),
('BR057', 'Le Mineral', 2500, 'JN001', 1, 0),
('BR090', 'Natrium Hipoklorit', 450000, 'JN004', 1, 0),
('BR097', 'PC LG', 2000000, 'JN002', 1, 0),
('BR099', 'Natrium Hipoklorit', 700000, 'JN001', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `kode_jabatan` varchar(5) NOT NULL,
  `nama_jabatan` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`kode_jabatan`, `nama_jabatan`, `keterangan`, `flag`) VALUES
('', '', '', 1),
('JB002', 'Admin', 'Operasional', 1),
('JB003', 'Junior Programmer', 'Programmer', 1),
('JB007', 'Staff', 'Ketua', 1),
('JB009', 'Junior Programmer', 'Programmer', 1),
('JB017', 'Admin', 'Operasional', 1),
('JB019', 'Office Boy', 'Oprasional', 1),
('JB021', 'Pegawai', 'Operasional', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_barang`
--

CREATE TABLE `jenis_barang` (
  `kode_jenis` varchar(5) NOT NULL,
  `nama_jenis` varchar(100) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_barang`
--

INSERT INTO `jenis_barang` (`kode_jenis`, `nama_jenis`, `flag`) VALUES
('JN001', 'Alat Tulis Kantor', 1),
('JN002', 'Perangkat Keras', 1),
('', '', 1),
('JN003', 'Perangkat Lunak', 1),
('JN004', 'Bahan Kimia', 1),
('JN001', 'Alat Tulis Kantor', 1),
('JN008', 'Minuman', 1),
('JN008', 'Minuman', 1),
('JN021', 'ATM', 1);

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE `karyawan` (
  `nik` varchar(10) NOT NULL,
  `nama_lengkap` varchar(150) NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jenis_kelamin` varchar(1) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  `kode_jabatan` varchar(5) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`nik`, `nama_lengkap`, `tempat_lahir`, `tgl_lahir`, `jenis_kelamin`, `alamat`, `telp`, `kode_jabatan`, `flag`) VALUES
('', '', '', '1959-03-01', 'L', '', '', 'JB002', 1),
('11111111', 'Rahma', 'Lampung', '0000-00-00', 'L', 'uhuhuygygtgyu', '', 'JB002', 1),
('1902003', 'Langgeng Kumboro', 'Lebak', '1998-12-08', 'L', 'RSN Waduk Pluit No. 302\r\nKec. Penjarngan - Jakarta Utara', '0815 1124 7338', 'JB003', 1),
('1902007', 'Langgeng Kumboro', 'Lebak', '1998-12-08', 'L', 'RSN WADUK PLUIT NO 302', '081511247338', 'JB003', 1),
('1902008', 'Rahmad', 'Yogyakarta', '1995-03-01', 'L', 'Jl. Merdeka Barat', '0872 1723 1921', 'JB007', 1),
('1902013', 'Jajang', 'Bandung', '2000-03-01', 'L', 'Dayeh Kolot', '0877718291', 'JB003', 1),
('1902083', 'Udin', 'Tokyo', '1979-03-01', 'L', 'Hiroshima', '0879 990 778', 'JB003', 1),
('1902098', 'uhuhhh', 'uu', '1959-03-01', 'L', 'huhiiiiiiiiihuu', '081511247338', 'JB002', 1),
('1902112', 'Maesaroh', 'Jawa Barat', '2000-03-01', 'P', 'Jl. Kawah Hejo', '08771 1278 118', 'JB002', 1),
('190283', 'Langgeng Ku', 'Lampung', '0000-00-00', 'L', 'Jl. Teunku Umar', '', 'JB007', 1),
('1902889', 'Usma', 'Lampung', '1959-03-01', 'L', 'gang maung', '081511247338', 'JB003', 1),
('1902903', 'Kumboro', 'Maluku', '0000-00-00', 'L', 'Sol', '', 'JB002', 1),
('1928001', 'Ahmed Liadri', 'Sumatra Barat', '1959-03-01', 'L', 'ffafas', '0856463211', 'JB002', 1);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id_user` int(11) NOT NULL,
  `nik` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(32) NOT NULL,
  `tipe` int(11) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id_user`, `nik`, `email`, `password`, `tipe`, `flag`) VALUES
(1, '1704421300', 'user@gmail.com', '24c9e15e52afc47c225b757e7bee1f9d', 2, 1),
(2, 'user2', 'user2@gmail.com', '7e58d63b60197ceb55a1c487989a3720', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pejualan-detail`
--

CREATE TABLE `pejualan-detail` (
  `id_jual_d` int(11) NOT NULL,
  `id_jual_h` int(11) NOT NULL,
  `kode_barang` varchar(5) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` float NOT NULL,
  `jumlah` float NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_detail`
--

CREATE TABLE `pembelian_detail` (
  `id_pembelian_d` int(11) NOT NULL,
  `id_pembelian_h` int(11) NOT NULL,
  `kode_barang` varchar(5) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian_detail`
--

INSERT INTO `pembelian_detail` (`id_pembelian_d`, `id_pembelian_h`, `kode_barang`, `qty`, `harga`, `jumlah`, `flag`) VALUES
(1, 59, 'BR001', 2, 1200000, 2400000, 1),
(2, 59, 'BR001', 2, 1200000, 2400000, 1),
(3, 59, 'BR001', 2, 1200000, 2400000, 1),
(4, 59, 'BR001', 2, 1200000, 2400000, 1),
(5, 59, 'BR001', 2, 1200000, 2400000, 1),
(6, 59, 'BR001', 2, 1200000, 2400000, 1),
(7, 60, 'BR001', 7, 1200000, 8400000, 1),
(8, 60, 'BR001', 7, 1200000, 8400000, 1),
(9, 60, 'BR001', 7, 1200000, 8400000, 1),
(10, 60, 'BR001', 7, 1200000, 8400000, 1),
(11, 60, 'BR001', 7, 1200000, 8400000, 1),
(12, 61, 'BR001', 2, 4500000, 9000000, 1),
(13, 61, 'BR001', 2, 4500000, 9000000, 1),
(14, 61, 'BR001', 2, 4500000, 9000000, 1),
(15, 62, '22231', 7, 1200000, 8400000, 1),
(16, 62, '22231', 7, 1200000, 8400000, 1),
(17, 62, '22231', 7, 1200000, 8400000, 1),
(18, 63, '22231', 7, 1200000, 8400000, 1),
(19, 64, 'BR007', 9, 800000, 7200000, 1),
(20, 64, 'BR007', 9, 800000, 7200000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_header`
--

CREATE TABLE `pembelian_header` (
  `id_pembelian_h` int(11) NOT NULL,
  `no_transaksi` varchar(10) NOT NULL,
  `tanggal` date NOT NULL,
  `kode_supplier` varchar(5) NOT NULL,
  `approved` int(11) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian_header`
--

INSERT INTO `pembelian_header` (`id_pembelian_h`, `no_transaksi`, `tanggal`, `kode_supplier`, `approved`, `flag`) VALUES
(50, 'TR0977271', '2019-02-21', 'SP002', 0, 0),
(51, 'TR0977279', '2019-02-21', 'SP011', 0, 0),
(52, '', '2019-02-21', '', 0, 0),
(53, 'TR0912001', '2019-02-21', 'SP002', 0, 0),
(54, 'TR45', '2019-02-21', 'SP002', 0, 0),
(55, 'TR0977271', '2019-02-21', 'SP009', 0, 0),
(56, 'TR0977271', '2019-02-21', 'SP009', 0, 0),
(57, '', '2019-02-21', 'SP002', 1, 1),
(58, '123', '2019-02-21', 'SP002', 1, 1),
(59, '123', '2019-02-21', 'SP002', 1, 1),
(60, 'TR0977271', '2019-03-21', 'SP002', 1, 1),
(61, 'TR0977271', '2019-03-21', 'SP002', 1, 1),
(62, 'TR0977277', '2019-03-26', 'SP002', 1, 1),
(63, 'TR7771', '2019-03-26', 'SP002', 1, 1),
(64, 'TR0977276', '2019-03-26', 'SP002', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_header`
--

CREATE TABLE `penjualan_header` (
  `id_jual_h` int(11) NOT NULL,
  `no_transaksi` varchar(10) NOT NULL,
  `tanggal` date NOT NULL,
  `approved` int(11) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `kode_supplier` varchar(5) NOT NULL,
  `nama_supplier` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`kode_supplier`, `nama_supplier`, `alamat`, `telp`, `flag`) VALUES
('SP002', 'PT. Amarta Indah Otsuka', 'Pasuruan 67172, Jawa Timur', '08001687852', 1),
('SP004', 'PT. Senosa Makmur Tbk', 'Jl. Raya Gadog 12', '021 0291 9912', 1),
('SP009', 'Samsung Indonesia', 'Cikarang', '021 7777 9999', 1),
('SP011', 'PT. Garda Terdepan Utama', 'Jl. Mangga Besar Kav. 9A', '021 778 775', 1),
('SP012', 'PT. Garda', 'Jl. Mangga Besar Kav. 9C', '0877 8191 9013', 1),
('SP094', 'PT.Graha Asia Pasific', 'Jl. Pluit Raya No.9D', '0213 991 1198', 1),
('SP982', 'Harapan Seha', 'Jl. Temabaga Mas No.7ABC', '0214 786 443', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`kode_barang`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`kode_jabatan`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`nik`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `pejualan-detail`
--
ALTER TABLE `pejualan-detail`
  ADD PRIMARY KEY (`id_jual_d`);

--
-- Indexes for table `pembelian_detail`
--
ALTER TABLE `pembelian_detail`
  ADD PRIMARY KEY (`id_pembelian_d`);

--
-- Indexes for table `pembelian_header`
--
ALTER TABLE `pembelian_header`
  ADD PRIMARY KEY (`id_pembelian_h`);

--
-- Indexes for table `penjualan_header`
--
ALTER TABLE `penjualan_header`
  ADD PRIMARY KEY (`id_jual_h`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`kode_supplier`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pejualan-detail`
--
ALTER TABLE `pejualan-detail`
  MODIFY `id_jual_d` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pembelian_detail`
--
ALTER TABLE `pembelian_detail`
  MODIFY `id_pembelian_d` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `pembelian_header`
--
ALTER TABLE `pembelian_header`
  MODIFY `id_pembelian_h` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `penjualan_header`
--
ALTER TABLE `penjualan_header`
  MODIFY `id_jual_h` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
